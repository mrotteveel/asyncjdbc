# asyncjdbc code style

The code style applied by asyncjdbc is based on the [Google Java Style Guide][1].

## Deviations from Google Java Style Guide

None at this time

## Style definitions

To support formatting in your IDE, we include style definitions for:

- Eclipse: `config/checkstyle/asyncjdbc-eclipse-java-style.xml`
- IntelliJ: `config/checkstyle/asyncjdbc-intellij-java-style.xml`

These definitions are based on the definitions from <https://github.com/google/styleguide>

## Checkstyle

The project uses [checkstyle][2] to enforce the style for the `adbaoverjdbc` module. The `adba` 
module is excluded as that's a copy from the module developed as part of Java.

The checkstyle.xml is based on `google_checks.xml` as included with checkstyle.

We allow suppression comments in the form of `CHECKSTYLE:OFF` ... `CHECKSTYLE:ON`, or
`CHECKSTYLE IGNORE <check> FOR NEXT <n> LINES`, but be prepared to explain the necessity of a 
suppression.

 [1]: https://google.github.io/styleguide/javaguide.html
 [2]: http://checkstyle.sourceforge.net/
