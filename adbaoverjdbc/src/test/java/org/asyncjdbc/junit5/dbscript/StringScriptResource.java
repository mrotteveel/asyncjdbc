/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.asyncjdbc.junit5.dbscript;

import static java.util.Objects.requireNonNull;

import java.io.Reader;
import java.io.StringReader;

/**
 * Script resource from a string.
 */
public final class StringScriptResource implements ScriptResource {

  private final String label;
  private final String script;

  /**
   * Creates a script resource for {@code script}.
   *
   * @param label Short label for the script
   * @param script Script
   */
  public StringScriptResource(String label, String script) {
    this.label = requireNonNull(label, "label");
    this.script = requireNonNull(script, "script");
  }

  @Override
  public Reader getScriptReader() {
    return new StringReader(script);
  }

  @Override
  public String getLabel() {
    return label;
  }

}
