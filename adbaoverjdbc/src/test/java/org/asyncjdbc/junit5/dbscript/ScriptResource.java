/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.asyncjdbc.junit5.dbscript;

import java.io.IOException;
import java.io.Reader;

/**
 * API for providing scripts to {@link DbSetupExtension}.
 */
public interface ScriptResource {

  /**
   * Returns a reader with the script.
   *
   * <p>Implementations must support multiple invocations of this method, returning a new reader for
   * each invocation.
   *
   * @return Reader with the script
   * @throws IOException For errors providing the script (eg unable to open underlying resource)
   */
  Reader getScriptReader() throws IOException;

  /**
   * Short label to identify the script in logging.
   *
   * @return Label for the script
   */
  String getLabel();
  
}
