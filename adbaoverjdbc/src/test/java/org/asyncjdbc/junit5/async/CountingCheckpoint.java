/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Portions Copyright (c) 2018 Red Hat, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.asyncjdbc.junit5.async;

import static java.util.Objects.requireNonNull;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 * Checkpoints that count the number of flag invocations.
 *
 * <p>Parts of this implementation have been derived from
 * <a href="https://github.com/vert-x3/vertx-junit5">vertx-junit5</a>
 */
class CountingCheckpoint implements Checkpoint {

  private final String name;
  private final AtomicInteger remainingPasses;
  private final Consumer<Checkpoint> satisfactionTrigger;

  private CountingCheckpoint(String name, int requiredNumberOfPasses,
      Consumer<Checkpoint> satisfactionTrigger) {
    this.name = name;
    if (requiredNumberOfPasses <= 0) {
      throw new IllegalArgumentException("A checkpoint needs at least 1 pass");
    }
    this.satisfactionTrigger = requireNonNull(satisfactionTrigger, "satisfactionTrigger");
    remainingPasses = new AtomicInteger(requiredNumberOfPasses);
  }

  static CountingCheckpoint laxCountingCheckpoint(String checkPointName, int requiredNumberOfPasses,
      Consumer<Checkpoint> satisfactionTrigger) {
    return new CountingCheckpoint(checkPointName, requiredNumberOfPasses, satisfactionTrigger);
  }

  @Override
  public String name() {
    return name;
  }

  @Override
  public void flag() {
    int currentRemainingPasses = remainingPasses.decrementAndGet();
    if (currentRemainingPasses == 0) {
      satisfactionTrigger.accept(this);
    }
  }
}
