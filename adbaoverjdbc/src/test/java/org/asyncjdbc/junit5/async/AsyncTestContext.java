/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Portions Copyright (c) 2018 Red Hat, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.asyncjdbc.junit5.async;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import org.opentest4j.AssertionFailedError;

/**
 * A test context to wait on the outcomes of asynchronous operations.
 *
 * <p>Parts of this implementation have been derived from
 * <a href="https://github.com/vert-x3/vertx-junit5">vertx-junit5</a>
 */
public class AsyncTestContext {

  private Throwable throwableReference = null;
  private final CountDownLatch releaseLatch = new CountDownLatch(1);
  private final AtomicBoolean inProgress = new AtomicBoolean(true);
  private final Set<Checkpoint> checkpoints = new HashSet<>();

  /**
   * Check if the context has been marked has failed or not.
   *
   * @return {@code true} if the context has failed, {@code false} otherwise.
   */
  public synchronized boolean failed() {
    return throwableReference != null;
  }

  /**
   * Give the cause of failure.
   *
   * @return the cause of failure, or {@code null} if the test context hasn't failed.
   */
  public synchronized Throwable causeOfFailure() {
    return throwableReference;
  }

  /**
   * Check if the context has completed.
   *
   * @return {@code true} if the context has completed, {@code false} otherwise.
   */
  public synchronized boolean completed() {
    return !failed() && releaseLatch.getCount() == 0;
  }

  /**
   * Complete the test context immediately.
   */
  public synchronized void completeNow() {
    inProgress.set(false);
    releaseLatch.countDown();
  }

  /**
   * Make the test context fail immediately, making the corresponding test fail.
   *
   * @param t the cause of failure.
   */
  public synchronized void failNow(Throwable t) {
    Objects.requireNonNull(t, "The exception cannot be null");
    if (!completed()) {
      // TODO Allow collection of multiple failures
      throwableReference = t;
      completeNow();
    }
  }

  /**
   * Make the test context fail immediately, making the corresponding test fail.
   *
   * @param reason reason of the failure
   */
  public void failNow(String reason) {
    failNow(new AssertionFailedError(reason));
  }

  private synchronized void checkpointSatisfied(Checkpoint checkpoint) {
    checkpoints.remove(checkpoint);
    if (!inProgress.get() && checkpoints.isEmpty()) {
      completeNow();
    }
  }

  /**
   * Create a checkpoint.
   *
   * @return a checkpoint that requires 1 pass.
   */
  public Checkpoint checkpoint(String checkPointName) {
    return checkpoint(checkPointName, 1);
  }

  /**
   * Create a checkpoint.
   *
   * @param requiredNumberOfPasses the required number of passes to validate the checkpoint.
   * @return a checkpoint that requires several passes.
   */
  public synchronized Checkpoint checkpoint(String checkPointName, int requiredNumberOfPasses) {
    CountingCheckpoint checkpoint = CountingCheckpoint
        .laxCountingCheckpoint(checkPointName, requiredNumberOfPasses, this::checkpointSatisfied);
    checkpoints.add(checkpoint);
    return checkpoint;
  }

  /**
   * Allow verifications and assertions to be made.
   *
   * <p>This method allows any assertion API to be used. The semantic is that the verification is
   * successful when no exception is being thrown upon calling {@code block}, otherwise the context
   * fails with that exception.
   *
   * @param block a block of code to execute.
   * @return this context.
   */
  public AsyncTestContext verify(Runnable block) {
    Objects.requireNonNull(block, "The block cannot be null");
    try {
      block.run();
    } catch (Throwable t) {
      failNow(t);
    }
    return this;
  }

  /**
   * Wait for the completion of the test context.
   *
   * <p>This method is automatically called by the {@link AsyncTestExtension} when using parameter
   * injection of {@link AsyncTestContext}. You should only call it when you instantiate this class
   * manually.
   *
   * @param timeout the timeout.
   * @param unit the timeout unit.
   * @return {@code true} if the completion or failure happens before the timeout has been reached,
   *     {@code false} otherwise.
   * @throws InterruptedException when the thread has been interrupted.
   */
  public boolean awaitCompletion(long timeout, TimeUnit unit) throws InterruptedException {
    inProgress.set(false);
    return failed() || releaseLatch.await(timeout, unit);
  }

  /**
   * Names of the checkpoints that were not completed.
   *
   * @return Collection of checkpoint names
   */
  public synchronized Collection<String> incompleteCheckpoints() {
    return checkpoints.stream()
        .map(Checkpoint::name)
        .collect(Collectors.toList());
  }

}
