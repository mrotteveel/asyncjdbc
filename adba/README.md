The code in this module was copied from the JDK sandbox project. Once ADBA is
included in Java, it can be removed.

Contrary to the main project, ADBA is licensed under the GPL v2 with Classpath 
exception.